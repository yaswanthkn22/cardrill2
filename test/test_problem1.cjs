const inventory = require("../data.cjs");
const get_car_id_33 = require("../problem1.cjs");

const result1 = get_car_id_33(inventory,33);
console.log(result1);
console.log(`Car 33 is a ${result1.car_year} ${result1.car_make} ${result1.car_model}`);