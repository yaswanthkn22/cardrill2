
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. 
//Using the array you just obtained from the previous problem, 
//find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function get_numbercars_before_2000 (inventory = []){
    
    if(Array.isArray(inventory) ){
        let older_cars = inventory.filter((car)=> {
            return car.car_year < 2000;
        });
        console.log(older_cars.length)
        return older_cars;
    }else return [];

}

module.exports = get_numbercars_before_2000;