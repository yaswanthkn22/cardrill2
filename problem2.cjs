/* ==== Problem #2 ====
 The dealer needs the information on the last car in their inventory. 
 Execute a function to find what the make and model of the last car in the inventory is?
 Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*" */


function get_last_car (inventory) {


    if(Array.isArray(inventory)){
    inventory.sort((a,b)=>{
        if(a.id > b.id){
            return 1;
        }else if(a.id < b.id){
            return -1;
        }else return 0;
    });
    return inventory[inventory.length-1];
}else return {};
}

module.exports =  get_last_car