
// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory. 
// Execute a function and return an array that only contains BMW and Audi cars. 
// Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


function get_bmw_audi_cars (inventory = []){

    if(Array.isArray(inventory)){

        let bmw_audi_cars = inventory.filter((car) => {
            if (car.car_make.toUpperCase() === 'AUDI' || car.car_make.toUpperCase() === 'BMW'){
                return true;
            }else return false;
        }).map((item)=>{
                return JSON.stringify(item);
        });
        console.log(bmw_audi_cars);
        return bmw_audi_cars;
    }else return [];
}


module.exports = get_bmw_audi_cars;