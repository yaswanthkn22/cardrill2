

/* The dealer can't recall the information for a car with an id of 33 on his lot. 
 Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
 Then log the car's year, make, and model in the console log in the format of:
 "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"  */

function get_car_id_33(inventory,id){
    if(Array.isArray(inventory) && Number.isInteger(id)){
        const array_filtered = inventory.filter((item)=> item.id === id);
        return (array_filtered.length)?array_filtered[0]:[];
    }else return [];
    
}

module.exports = get_car_id_33;